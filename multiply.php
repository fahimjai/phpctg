<?php
class Multiply{

//define a function
    function multiply2numbers($number1,$number2)
    {
        $result=$number1*$number2;
        return $result;
    }
}



class Displayer{    //Formatter,Outputter

    function displaySimple($story){
        //print add result
        echo $story;

    }
    function displayH1($story){
        echo "<h1>";
        echo $story;
        echo "</h1>";
    }
    function displayPre($story){
        echo "<hr />";
        echo "<pre>";
        echo $story;
        echo "</pre>";
        echo "<hr />";

    }

}

$multiply1=new Multiply();  //instanciating (creating) object($addition1) form class/plan(Addition)
$displayer1=new Displayer();

$result="The Multiply of two numbers is : ".$multiply1->multiply2numbers($_POST['number1'],$_POST['number2']);

$displayer1->displayPre($result);
$displayer1->displayH1($result);
$displayer1->displaySimple($result);


?>