<?php
class Subtract{

//define a function
    function subtract2numbers($number1,$number2)
    {
        $result=$number1-$number2;
        return $result;
    }
}



class Displayer{    //Formatter,Outputter

    function displaySimple($story){
        //print add result
        echo $story;

    }
    function displayH1($story){
        echo "<h1>";
        echo $story;
        echo "</h1>";
    }
    function displayPre($story){
        echo "<hr />";
        echo "<pre>";
        echo $story;
        echo "</pre>";
        echo "<hr />";

    }

}

$subtract1=new Subtract();  //instanciating (creating) object($addition1) form class/plan(Addition)
$displayer1=new Displayer();

$result="The subtraction of two numbers is : ".$subtract1->subtract2numbers($_POST['number1'],$_POST['number2']);

$displayer1->displayPre($result);
$displayer1->displayH1($result);
$displayer1->displaySimple($result);


?>